#!/bin/sh

add_a_user()
{
	USER=$1
	PASSWORD=$2
	# First arg is user, second arg is password
	# We now drop these two arguments as the rest will be comments 
	# Can call $@ for all args knowing they will all be comments
	shift; shift;
	COMMENTS=$@
	echo "Adding user $USER ..."
	echo useradd -c "$COMMENTS" $USER
	echo passwd $USER $PASSWORD
	echo "Added user $USER ($COMMENTS) with pass $PASSWORD"
}

# Main body of script

echo "Start of script..."
add_a_user bob letmein Bob Holness the presenter
add_a_user fred badpassword Fred Durst the \"singer\"
add_a_user bilko worsepassword Sgt. Bilko the role model
echo "End of script..."
