# Exercise 9
\
Linux and Windows use different systems for declaring the end of a line. 
Windows uses the _carriage return plus line feed_ system, otherwise known as **CRLF** or **CR/LF**. 
Linux uses the new line character (\n). This can lead to problems when using files accross these two systems
and is something you should be aware of for your own sanity. 
