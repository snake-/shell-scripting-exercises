#!/bin/sh
mkdir old 
cp *.txt old
for FILE in old/*.txt
do
	mv -- "$FILE" "${FILE%.txt}.old"
done
