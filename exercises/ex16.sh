#/bin/sh

. ./ex15.lib
echo Welcome to the
cat ascii_art.txt
while :
do
	echo "------------------------"
	echo "Please select an option:"
	echo "------------------------"
	echo "1.) Add new record"
	echo "2.) View all records"
	echo "3.) Search records"
	echo "------------------------"
	read IN
	case $IN in
		1)
			add_record
			echo "Would you like to enter another record? (Y/n)"
			read INPUT
			record_prompt_commas $INPUT
			;;
		2)
			display_records
			;;
		3)
			echo "Search :"
			read SEARCH
			search_records $SEARCH
			;;
		*)
			echo "Error invalid command please try again"
			;;
	esac
done
